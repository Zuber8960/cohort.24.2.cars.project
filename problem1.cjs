
// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"


const inventory = require('./cars.cjs');
// console.log(inventory);

function problem1(inventory, id) {
    if( !Array.isArray(inventory)  || inventory === undefined || inventory.length === 0 || id == undefined || typeof(id) !== "number"){
        return [];
    }
    for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].id === id) {
            return inventory[index];
        }
    }
    return [];
}
// console.log(problem1(inventory,33));

module.exports = problem1;

