
// // ==== Problem #2 ====
// // The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 
// "Last car is a *car make goes here* *car model goes here*"


const inventory = require('./cars.cjs');
// console.log(inventory);

function problem2(inventory) {
    if( !Array.isArray(inventory)  || inventory === undefined || inventory.length === 0){
        return [];
    }
    const lastInventory = inventory.length - 1;
    return lastInventory;
}
// console.log(problem2(inventory));

module.exports = problem2;


