
// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.


const inventory = require('./cars.cjs');
// console.log(inventory);


function problem3(inventory){
    if( !Array.isArray(inventory)  || inventory === undefined || inventory.length === 0){
        return [];
    }
    for(let index = 1; index<inventory.length; index++){
        if(inventory[index].car_model.toLowerCase()<inventory[index-1].car_model.toLowerCase()){
            [inventory[index],inventory[index-1]] = [inventory[index-1], inventory[index]];
            index=0;
        }
    }

    return inventory;
}

// console.log(problem3(inventory));


module.exports = problem3;












