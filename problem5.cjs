
// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


const inventory = require('./cars.cjs');
const getCarYears = require('./problem4.cjs');

const allCarsYears = getCarYears(inventory);
// console.log(allCarsYears);
const arr = [];

function problem5(inventory) {
    for (let index = 0; index < inventory.length; index++) {
        if (allCarsYears.includes(inventory[index].car_year)) {
            arr.push(inventory[index]);
        }
    };
    return arr;
}

// console.log(problem5(inventory).length);

module.exports = problem5;




