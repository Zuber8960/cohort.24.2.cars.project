
// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.


const inventory = require('./cars.cjs');
// console.log(inventory);

const arr = [];

function problem6(inventory) {
    if( !Array.isArray(inventory)  || inventory === undefined || inventory.length === 0){
        return [];
    }
    for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].car_make === 'BMW' || inventory[index].car_make === 'Audi') {
            arr.push(inventory[index]);
        }
    };
    return arr;
}

// console.log(problem6(inventory));


module.exports = problem6;



