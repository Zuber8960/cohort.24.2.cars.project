const problem1 = require('../problem1.cjs');
const inventory = require('../cars.cjs');
const id = 33;
// const result = problem1(inventory,id);
// const result = problem1(new String("hello"), 33);
// const result = problem1({id: 33, name: "Test", length: 10}, 33);
result = problem1(inventory, 99)

if(result.length === 0){
    console.log(result);
}else{
    console.log(`Car ${result.id} is a ${result.car_year} ${result.car_make} ${result.car_model}`);
}

