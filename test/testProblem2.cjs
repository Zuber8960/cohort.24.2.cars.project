const problem2 = require('../problem2.cjs');
const inventory = require('../cars.cjs');

const result = problem2(inventory);

if(!result){
    console.log([]);
}else{
    console.log(`Last car is a ${result.car_make} ${result.car_model}.`);
}
